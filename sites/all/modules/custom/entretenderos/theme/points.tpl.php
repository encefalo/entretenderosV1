<?php if($items['points'][0]["points"]):?>
  <?php if($items['points'][0]["points"] >= $items['max_points']):?>
    <img class="img-responsive" src="/sites/all/themes/entretenderos_theme/img/oro.png">
    <div class="text-center"><?php print $items['points'][0]["points"] . '/' . $items['max_points']*3;?> Puntos</div>
  <?php elseif(((int)$items['points'][0]["points"] <= (int)$items['max_points']/2) && ((int)$items['points'][0]["points"] >= (int)$items['max_points']/3)):?>
    <img class="img-responsive" src="/sites/all/themes/entretenderos_theme/img/plata.png">
    <div class="text-center"><?php print $items['points'][0]["points"] . '/' . $items['max_points']*3;?> Puntos</div>
  <?php else: ?>
    <img class="img-responsive" src="/sites/all/themes/entretenderos_theme/img/bronce.png">
    <div class="text-center"><?php print $items['points'][0]["points"] . '/' . $items['max_points']*3;?> Puntos</div>
  <?php endif?>
<?php else : ?>
  <img class="img-responsive" src="/sites/all/themes/entretenderos_theme/img/bronce.png">
  <div class="text-center">0/<?php print $items['max_points'];?> Puntos</div>
<?php endif ?>
