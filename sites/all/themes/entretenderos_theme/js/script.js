(function($, Drupal){
  // I want some code to run on page load, so I use Drupal.behaviors
  Drupal.behaviors.redds = {
    attach:function(context, settings){
      $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
      }
      $('.page-user .field-name-ds-user-picture img').addClass('img-circle');
      $('.node-type-comunidad .field-name-ds-user-picture img').addClass('img-circle');
      $('#block-views-user-block .user-picture img ').addClass('img-circle');
      $("div:contains('No tiene permiso para visitar esta página.')").html(function(buscayreemplaza, reemplaza) {
        return reemplaza.replace('No tiene permiso para visitar esta página.', '');
      });
      if($.urlParam('sesssion') === "true"){
        $('#autologin').click();
      }
      $('.form-item-comment-body-und-0-value label').text("Comentario");
      jQuery('.field-name-field-tags a ').each(function( index ) {
        jQuery(this).text("#"+ jQuery( this ).text());
      });
      $('.taxonomy-term.vocabulary-tags.view-mode-full .field-name-title h2').text("#" + $('.taxonomy-term.vocabulary-tags.view-mode-full .field-name-title h2').text());
    }
  };
}(jQuery, Drupal));
